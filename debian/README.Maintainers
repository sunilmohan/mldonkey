*********************************************
* MLDonkey package maintenance, README file *
*********************************************

The purpose of this file is to define how the MLDonkey package should be
handled. This document if not a reference, it is just a way to write down the
way different maintainers of this package should work to have the best
efficiency.

1. Notational convention

When talking about the whole software, you should use "MLDonkey".

It is just for consistency...

2. Upstream

Link to MLDonkey project:
http://savannah.nongnu.org/projects/mldonkey/

When submitting a bug report, you should:
- use a valid savannah account
- set the binary origin to "Debian package"
- copy/paste useful information you have gathered while talking with bug
  submitter
- give a link to the original bug. Example:
  This bug comes from the Debian BTS:
  http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=272405
- add mail notification to the original BTS entry:
  272405@bugs.debian.org added by gildor (Debian BTS entry)
- set the bug as forwared to upstream (using mldonkey-bugs@nongnu.org as email
  address)

For the sake of simplicity, MLDonkey should stay close to uptream source. For
example, SpiralVoice provides a lot of good patches. Most of the time I take a
look at them, but don't apply them. Reasons for this are: SpiralVoice is a
member of MLDonkey project -- his modifications will be included in the next
release --, it is difficult to report bugs when the original upstream source has
been heavily modified. Trying to stay close to official release, lets report bug
in a more simple manner, because you are dealing with the official version.

The upstream tarball of MLDonkey is not a pure vanilla upstream source. You need
to apply debian/utils/purify_mldonkey. This script removes some files which are
related to the FASTTRACK problem (see the file FASTTRACK).

3. Utils

There are a lot of utils program in debian/utils. They are all written using
OCaml. Those utilies are made to manipulate different data structures of
MLDonkey (password file creation, configuration file handling...).

For now, I prefer to stay consistent with the rest of the package and not use
other language than:
- ocaml, because it is already a "BuildDepends" of the package,
- sh, because it is mandatory to have this interpreter.

In particular, perl and python should not be used. Because it will cause a lot
of dependencies (and make it even more long to build in a chroot/uml pbuilder).
It is also a question of personnal motivation.

4. Subversion

As usual:
- if the changes are minor, they can be done directly in the trunk
- if this are bigger changes, you should create a branch

The trunk should always be able to build.

The subversion repository layout should follow the one defined by the OCaml 
Maintainers Task Force.

Link to OCaml Maintainer Task Force:
http://pkg-ocaml-maint.alioth.debian.org/

Link to MLDonkey subversion:
http://svn.debian.org/wsvn/pkg-ocaml-maint/trunk/packages/mldonkey/trunk/?rev=0&sc=0

Off course, the repository is hosted on alioth. Account for this project should
be asked (politely) on debian-ocaml-maint@lists.debian.org.

5. Building the package

To build the package, you should checkout 
svn+ssh://svn.debian.org/svn/pkg-ocaml-maint/trunk/tools/opkg-buildpackage.

Modification to the package, should be done using dpatch, and once validated,
the patch should be submitted upstream.

6. People

- Sylvain Le Gall <gildor@debian.org>:
  - takes care of any aspect regarding OCaml part of the package,
- Jeroen Van Wolffelaar <jvw@debian.org>:
  - takes care of the BTS, answer bug, forward bug to upstream, assign bug,
  - tests the package,
  - takes care of maintainer script
  - decides to do a release,
