
---------------------------------
*                               *
*      Password and user        *
*                               *
---------------------------------

1 Changing Password

MLDonkey natively support only two ways to set the password: through the GUI,
through the telnet interface.

To change the password through the telnet interface:
- you log into the telnet interface with the former user/password, by default
  this can be done using the command

  telnet localhost 4000

- you change the password with the auth command.

  telnet>auth user old-passwd
  telnet>add_user user new-passwd
  telnet>auth user new-passwd

You can also do the same through the GUI by providing at the beginning the
old password in Files->Settings->Password, connect to the core, change the
password in Files->Settings->Password.

2 Default parameter

At the beginning only one user exist in the system: admin. The default
password for this user is an empty string. In order to connect through the
telnet interface, you need to type:

  telnet>auth admin ""

To login through the GUI:
- Files->Settings->Password (empty)
- Files->Settings->Password admin

To login through HTTP:
- User: admin
- Password: (empty)

3 Adding user

There is only one way to add new user: through the telnet interface.  You
need to invoke the command add_user. See help in the telnet interface about
this command.

 -- Sylvain Le Gall <gildor@debian.org>  Sun, 02 Sep 2003 00:22:00 +0200

---------------------------------
*                               *
*     The FASTTRACK problem     *
*                               *
---------------------------------

See FASSTRACK.

In order to fix the problem coming with the FASTTRACK problem, i build the
program called mldonkey_files.

This program permits you to filter the files.ini to extract Fasttrack sources.
But you can also use with any other plugin:
- for fasttrack mldonkey_files --split Fasttrack -f0 files.ini -f1
  files.ini.fasttrack -f2 files.ini
- for edonkey mldonkey_files --split Donkey -f0 files.ini -f1
  files.ini.edonkey -f2 files.ini

 -- Sylvain Le Gall <gildor@debian.org>  Sun, 25 Sep 2003 00:33:00 +0200

----------------------
*                    *
* GTK2 interface     *
*                    *
----------------------

In order to make the new GTK2 interface to work, we need to use UTF-8
everywhere on the GUI side. This means in particular, you have to be sure that
every strings in the GUI configuration file is UTF-8 encoded. In particular,
you need to be sure that your login/password are UTF-8 encoded.

So you have two choice:
- use "iconv" to convert the file
- erase the file and recreate it.

I think the second method is the best.

 -- Sylvain Le Gall <gildor@debian.org>  Mon, 24 Aug 2004 23:23:00 +0200

----------------------
*                    *
* Force startup      *
*                    *
----------------------

Since the beginning of the package, a choice has been made to allow user to
choose to start MLDonkey or not at startup. This decision was made because
mldonkey is not an essential service and can be a really heavy
time/network/processor consumer.

However, it should be interesting to be able to start mldonkey, as configured
during the install after the boot ( ie when you know that you can start it ).
But the fact that you have configured the script not to start at startup,
prevents user to do that.

Some users proposed to link/not to link startup script from
/etc/rcX.d/SXXmldonkey-server to /etc/init.d/mldonkey-server. This way is a
violation of the debian policy ( at least i think so ).

I decide to let user be able to start the script using:
"/etc/init.d/mldonkey-server force-start".  This way, we are policy compliant
and we let user start mldonkey-server, when needed.

 -- Sylvain Le Gall <gildor@debian.org>  Wed, 27 Apr 2005 23:35:36 +0200

-----------------------
*                     *
* MLDonkey and groups *
*                     *
-----------------------

MLDonkey cannot handle more than one group when running. This limitation is
due to OCaml library not providing enough function for changing the groups. So
MLDonkey server will only be run using the user/primary group that is assigned
to the MLDonkey user.

For example:
$ cat /etc/passwd | grep mldonkey
mldonkey:x:102:1002::/var/lib/mldonkey:/bin/false
$ cat /etc/group | grep mldonkey
windows:x:1003:mldonkey
mldonkey:x:1002:

Won't made the process mldonkey run with group mldonkey and windows, as
expected!  It will run with user mldonkey and group mldonkey ( because it is
the primary group of mldonkey ).

So you have to choose the most appropriate primary group for mldonkey and
change the primary group of mldonkey (usermod(8)) to this group.

For example, if you need to write to a special partition (for example Windows
partition) and that only windows group user can do that, you should set the
primary group of mldonkey to "windows".

This bug has been sent to the ocaml team.

 -- Sylvain Le Gall <gildor@debian.org>  Fri, 29 Apr 2005 00:58:08 +0200

---------------------------------
*                               *
*      Mldonkey and chroot      *
*                               *
---------------------------------

This feature is not supported anymore. Use the scripts at your own risks.
They can be found at:

http://git.debian.org/?p=pkg-ocaml-maint/packages/mldonkey.git;a=tree;f=debian/utils;h=21dc9c07a9c32a7651620430d4457206d5fc454b;hb=HEAD

 -- Mehdi Dogguy <dogguy@pps.jussieu.fr>  Wed, 11 Mar 2009 16:57:46 +0100

Actually, there is an option to enable chroot for mldonkey.

Mick Kappenburg has created a script to create chroot for mldonkey. For now
this script need more extensive testing, but i think it could be use to run
more safely mldonkey.

 -- Sylvain Le Gall <gildor@debian.org>  Sun, 05 Jun 2005 23:25:00 +0200
